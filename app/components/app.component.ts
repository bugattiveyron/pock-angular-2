import {Component} from '@angular/core'

//Decorator
@Component({
  selector: 'my-app', 
  template: `
    <h1>Hello World</h1>
  `
})


//Expoe a classe para outras classes
export class AppComponent{

}
