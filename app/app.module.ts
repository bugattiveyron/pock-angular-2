import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./components/app.component";

//Criando um Decorator
//Imports: Importando moutros módulos
//Declarations: //Diretivas, Serviços ou Components
@NgModule({
  imports: [BrowserModule],
  declarations:[AppComponent],
  bootstrap: [AppComponent]
})

export class AppModule{
  
}
